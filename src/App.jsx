import {SeasonsDropdown} from "./components/SeasonsDropdown";
import {useState, useEffect} from "react";
import {TeamsAPI} from "./api/teams";
import {MatchEntry} from "./components/MatchEntry";
import {RankedTeams} from "./components/RankedTeams/RankedTeams";
import {SeasonEntry} from "./components/SeasonEntry";
import {SeasonsAPI} from "./api/seasons";
import {MatchesAPI} from "./api/matches";
import {Matches} from "./components/Matches/Matches";
import {Col, Container, Row} from "react-bootstrap";
import {TeamEntry} from "./components/TeamEntry";

export function App() {

    const [appState, setAppState] = useState({
        season: {seasonName:"", description:""},
        teams: [],
        alphaTeams: [],
        matches: [],
        seasons: [],
        selectedTeam: null
    });

    function doSetState(stateObj){
        setAppState( prevState => {
            return {...prevState, ...stateObj};
        })
    }

    function setSelectedTeam(team){
        console.log("setting parent App selected team to " + team)
        let obj = {}
        obj.selectedTeam = team
        doSetState(obj)
    }

    async function setCurrentSeason(selectedSeason){
        console.log("setting parent App season to " + selectedSeason.seasonName)

        let obj = {};
        obj.season = selectedSeason
        obj.teams = await TeamsAPI.fetchTeamsSortRanked(selectedSeason.seasonName)
        obj.alphaTeams = await TeamsAPI.fetchTeamsSortAlpha(selectedSeason.seasonName)
        obj.matches = await MatchesAPI.fetchMatches(selectedSeason.seasonName)
        doSetState(obj)
    }

    async function refreshTeams(seasonName) {
        console.log("refreshing teams view for season " + seasonName)

        let obj = {};
        obj.teams = await TeamsAPI.fetchTeamsSortRanked(seasonName)
        obj.alphaTeams = await TeamsAPI.fetchTeamsSortAlpha(seasonName)
        obj.matches = await MatchesAPI.fetchMatches(seasonName)
        doSetState(obj)
    }

    async function initSeason(newSeason) {
        console.log("refreshing for new season")

        const seasons = await SeasonsAPI.fetchSeasons()
        let season = newSeason ? newSeason : seasons[0]

        let obj = {};
        obj.seasons = seasons
        obj.season = season
        obj.teams = await TeamsAPI.fetchTeamsSortRanked(season.seasonName)
        obj.alphaTeams = await TeamsAPI.fetchTeamsSortAlpha(season.seasonName)
        obj.matches = await MatchesAPI.fetchMatches(season.seasonName)
        doSetState(obj)
    }
    useEffect(() => {
        initSeason()
    }, [])


    return (
        <>
            <Container>
                <Row>
                    <Col>
                        <h1>{appState.season.seasonName}</h1>
                        <h3>{appState.season.description}</h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <SeasonsDropdown seasons={appState.seasons} currentSeason={appState.season} setCurrentSeason={setCurrentSeason} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <SeasonEntry notifyNewSeason={initSeason} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <hr/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <RankedTeams teams={appState.teams} selectedTeam={appState.selectedTeam} notifySelectedTeam={setSelectedTeam}/>
                    </Col>
                    <Col>
                        <Matches matches={appState.matches} teams={appState.teams} selectedTeam={appState.selectedTeam}/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TeamEntry season={appState.season} notifyNewTeam={refreshTeams} />
                    </Col>
                    <Col>
                        <MatchEntry currentSeason={appState.season} teamsList={appState.alphaTeams} refreshTeams={refreshTeams} />
                    </Col>
                </Row>
            </Container>
            <hr/>

        </>
    )
}