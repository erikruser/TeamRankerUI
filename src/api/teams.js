import axios from "axios";

const RANKER_URL = "http://server-box.local:4001/team-ranker/"

export class TeamsAPI {

    static async fetchTeamsSortAlpha(season) {
        const response = await axios.get(RANKER_URL + season + "/teams/?sortAlpha=true")
        console.log("fetched teams from server (sort alpha): ", response.data)
        return response.data
    }

    static async fetchTeamsSortRanked(season) {
        const url = RANKER_URL + season + "/teams/?sortAlpha=false"
        const response = await axios.get(url)
        console.log("fetched teams from server (sort ranked): ", response.data)
        return response.data
    }

    static async addTeam(season, newTeam) {
        const url = RANKER_URL + season + "/teams/"
        await axios.post(url, newTeam)
    }
}