import axios from "axios";

const RANKER_URL = "http://server-box.local:4001/team-ranker/"

export class MatchesAPI {

    static async fetchMatches(season) {
        const response = await axios.get(RANKER_URL + season + "/matches/")
        console.log("fetched matches from server: ", response.data)
        return response.data
    }

    static async addMatch(season, match) {
        console.log("posting match")
        const response = await axios.post(RANKER_URL + season + "/matches/", match)
        console.log(response.data)
    }

}