import axios from "axios";

const RANKER_URL = "http://server-box.local:4001/team-ranker/"

export class SeasonsAPI {

    static async fetchSeasons() {
        const response = await axios.get(RANKER_URL)
        console.log("fetched seasons from server: ", response.data)
        return response.data
    }

    static async addSeason(newSeason) {
        console.log(newSeason)
        await axios.post(RANKER_URL, newSeason)
    }

}