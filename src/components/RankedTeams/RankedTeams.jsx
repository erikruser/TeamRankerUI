import s from "./style.module.css";
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

export function RankedTeams(props) {

    function round(num, precision) {
        if(isNaN(num)){
            num = 0
        }
        return Number(Math.round(num + "e+" + precision) + "e-" + precision);
    }


    function rowOnClickHandler(team) {
        console.log("row for team " + team.teamName + " clicked. Current team is " + props.selectedTeam?.teamName)
        if(props.selectedTeam == team) {
            console.log("notifying selected team as null")
            props.notifySelectedTeam(null)
        } else {
            console.log("notifying selected team as " + team.teamName)
            props.notifySelectedTeam(team)
        }
    }


    function teamRow(team, index) {
        console.log("generating row for team " + team.teamName)
        return (
            <TableRow key={index} hover onClick={() => rowOnClickHandler(team)} selected={team == props.selectedTeam}>
                <TableCell>{team.teamName}</TableCell>
                <TableCell>{team.teamCode}</TableCell>
                <TableCell>{team.wins}</TableCell>
                <TableCell>{team.losses}</TableCell>
                <TableCell>{team.wins + team.losses}</TableCell>
                <TableCell>{round(team.record, 4)}</TableCell>
                <TableCell>{round(team.weightedRecord, 4)}</TableCell>
            </TableRow>
        )
    }

    console.log("Building team view")

    return (
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell>Team Name</TableCell>
                            <TableCell>Short Name</TableCell>
                            <TableCell>Wins</TableCell>
                            <TableCell>Losses</TableCell>
                            <TableCell>Matches Played</TableCell>
                            <TableCell>Record</TableCell>
                            <TableCell>Weighted Record</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {props.teams.map( (team, index) => teamRow(team, index))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    )

}