import {useState} from "react";
import {TeamsAPI} from "../api/teams";

export function TeamEntry(props) {

    async function handleSubmit() {
        let newTeam = {teamName: newTeamName, teamCode: newTeamCode}
        await TeamsAPI.addTeam(props.season.seasonName, newTeam)
        props.notifyNewTeam(props.season.seasonName)
    }

    const [newTeamName, setNewTeamName] = useState();
    const [newTeamCode, setNewTeamCode] = useState();

    function nameHandleKeyUp(e) {
        setNewTeamName(e.target.value)
    }

    function codeHandleKeyUp(e) {
        setNewTeamCode(e.target.value)
    }

    return (
        <>
            <label>
                Team Name
                <input type="text" onKeyUp={nameHandleKeyUp}/>
            </label>
            <label>
                Team Code
                <input type="text" onKeyUp={codeHandleKeyUp}/>
            </label>
            <input type="submit" value="Add team" onClick={handleSubmit}/>
        </>
    )

}