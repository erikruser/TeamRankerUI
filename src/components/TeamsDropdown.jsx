

export function TeamsDropdown(props) {

    function handleOnChange(e) {
        props.setTeamSelection(e.target.value)
    }

    let teamList = props.teamsList
    // teamList.sort( (a,b) => {
    //     return a.teamName.localeCompare(b.teamName)
    // })

    const teamOptions = teamList
        .map( (team, index) => <option value={team.teamCode} key={index}>{team.teamName}</option>)

    return (
        <>
            <select onChange={handleOnChange}><option/>{teamOptions}</select>
        </>
    )

}