import {SeasonsAPI} from "../api/seasons";
import {useState} from "react";

export function SeasonEntry(props) {

    async function handleSubmit() {
        let newSeason = {seasonName: newSeasonName, description: newSeasonDescription}
        await SeasonsAPI.addSeason(newSeason)
        props.notifyNewSeason(newSeason)
    }

    const [newSeasonName, setNewSeasonName] = useState();
    const [newSeasonDescription, setNewSeasonDescription] = useState();
    function nameHandleKeyUp(e) {
        setNewSeasonName(e.target.value)
    }
    function descriptionHandleKeyUp(e) {
        setNewSeasonDescription(e.target.value)
    }

    return (
        <>
            <label>
                Season Name
                <input type="text" onKeyUp={nameHandleKeyUp}/>
            </label>
            <label>
                Season Description
                <input type="text" onKeyUp={descriptionHandleKeyUp}/>
            </label>
            <input type="submit" value="Add season" onClick={handleSubmit}/>
        </>
    )

}