import s from "./style.module.css"
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

export function Matches(props) {

    function matchRow(match, index) {
        console.log("generating row for match " + match.id, match)

        if(props.selectedTeam != null &&
            (props.selectedTeam.teamCode != match.winner && props.selectedTeam.teamCode != match.loser)) {
            console.log("winner: " + match.winner + " loser: " + match.loser + " selectedTeam: " + props.selectedTeam.teamCode)
            return (<></>)
        }

        const winner = props.teams.find(t => t.teamCode === match.winner)
        const loser = props.teams.find(t => t.teamCode === match.loser)

        console.log("teams in match: ", winner, loser)

        return (
            <TableRow key={index} hover>
                <TableCell>{match.group}</TableCell>
                <TableCell>{winner.teamName}</TableCell>
                <TableCell>{loser.teamName}</TableCell>
            </TableRow>
        )
    }

    console.log("building matches view")

    return (
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell>Match Group</TableCell>
                            <TableCell>Winner</TableCell>
                            <TableCell>Loser</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {props.matches.map( (match, index) => matchRow(match, index))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    )
}