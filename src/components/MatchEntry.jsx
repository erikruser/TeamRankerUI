import {TeamsDropdown} from "./TeamsDropdown";
import {useEffect, useState} from "react";
import {MatchesAPI} from "../api/matches";

export function MatchEntry(props) {

    const [winningTeam, setWinningTeam] = useState()
    const [losingTeam, setLosingTeam] = useState()

    function setWinnerSelection(selectedTeam) {
        setWinningTeam(props.teamsList.find(e => e.teamCode === selectedTeam))
    }

    function setLoserSelection(selectedTeam) {
        setLosingTeam(props.teamsList.find(e => e.teamCode === selectedTeam))
    }

    useEffect(() => {
        setWinningTeam(null)
        setLosingTeam(null)
    }, [props.teamsList])

    async function addMatch() {
        if(winningTeam && losingTeam && winningTeam !== losingTeam){
            console.log(winningTeam)
            console.log(losingTeam)
            let match = {winner: winningTeam.teamCode, loser: losingTeam.teamCode, group: group}
            console.log(match)
            await MatchesAPI.addMatch(props.currentSeason.seasonName, match)
            props.refreshTeams(props.currentSeason.seasonName)
        } else {
            console.log("match teams not set.")
        }
    }

    const [group, setGroup] = useState();
    function handleKeyUp(e) {
        setGroup(e.target.value)
    }

    return (
        <div>
            <label>
                Winner:
                <TeamsDropdown teamsList={props.teamsList} setTeamSelection={setWinnerSelection} />
            </label>
            <label>
                Loser:
                <TeamsDropdown teamsList={props.teamsList} setTeamSelection={setLoserSelection} />
            </label>
            <label>
                Match Group:
                <input type="text" onKeyUp={handleKeyUp} />
            </label>
            <button onClick={addMatch}>Add Match</button>
        </div>
    )

}