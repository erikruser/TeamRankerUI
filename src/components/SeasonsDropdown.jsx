
export function SeasonsDropdown(props) {


    function handleDropdownSelectionChange(e) {
        let selectedSeason = props.seasons[e.target.value]
        props.setCurrentSeason(selectedSeason)
    }

    const seasonsOptions = props.seasons.map((seasonObj, index) =>
        <option value={index} key={index} >
            {seasonObj.seasonName}
        </option>)

    console.log("Dropdown selected season is " + props.currentSeason.seasonName)
    let selectedIndex = props.seasons.findIndex(s => s.seasonName === props.currentSeason.seasonName)
    console.log("Dropdown selected index: " + selectedIndex)
    console.log("Seasons list: ", props.seasons)

    return (
        <>
            Season: <select
                onChange={handleDropdownSelectionChange}
                value={selectedIndex}
            >{seasonsOptions}</select>
        </>
    )

}